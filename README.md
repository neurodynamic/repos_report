# Purpose

Prints info on the status of all repos under a given directory.

    $ gem install repos_report

### How it works

To get a report on the status of repos in a directory:
```
rep2 [directory]
```